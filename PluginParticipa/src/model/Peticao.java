package model;

import java.util.ArrayList;

public class Peticao {
	private String titulo;
	private String motivo;
	private String descricao;
	private ArrayList<String> listaAssinaturas;
	private Double numeroAssinaturas;
	private String enviarPara; /* Para quem deve ser enviado essa petição*/
	
	public Peticao (String titulo, String motivo, String descricao) {
		listaAssinaturas = new ArrayList<String>();
		this.descricao = descricao;
		this.motivo = motivo;
		this.titulo = titulo;
	}
	
	
	public String getEnviarPara() {
		return enviarPara;
	}
	public void setEnviarPara(String enviarPara) {
		this.enviarPara = enviarPara;
	}


	public Double getNumeroAssinaturas() {
		return numeroAssinaturas;
	}
	public void setNumeroAssinaturas(Double numeroAssinaturas) {
		this.numeroAssinaturas = numeroAssinaturas;
	}

	public ArrayList<String> getListaAssinaturas() {
		return listaAssinaturas;
	}
	public void setListaAssinaturas(ArrayList<String> listaAssinaturas) {
		this.listaAssinaturas = listaAssinaturas;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
