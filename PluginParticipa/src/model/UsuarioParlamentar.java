package model;

import java.util.ArrayList;

public class UsuarioParlamentar extends Usuario{

	private String comissao;
	private String bandeiraPartidaria;
	
	public UsuarioParlamentar(String nome, ArrayList<String> telefones,
			String email, String comissao, String bandeiraPartidaria) {
		
		super(nome, telefones, email);
		this.bandeiraPartidaria = bandeiraPartidaria;
		this.comissao = comissao;
	}
	
	public String getComissao() {
		return comissao;
	}
	public void setComissao(String comissao) {
		this.comissao = comissao;
	}

	public String getBandeiraPartidaria() {
		return bandeiraPartidaria;
	}
	public void setBandeiraPartidaria(String bandeiraPartidaria) {
		this.bandeiraPartidaria = bandeiraPartidaria;
	}
	
}
