package model;

import java.util.ArrayList;

public class Usuario {
	private String nome;
	private String cpf;
	private String rg;
	private String posicaoSocial;
	private String email;
	private ArrayList<String> listaTelefones;
	private Double numeroAvaliacaoPositiva;
	private Double numeroAvalicaoNegativa;
	private Double numeroPostagens;
	private ArrayList<Reivindicacao> listaReivindicacoes;
	
	public Usuario (String nome, ArrayList<String> telefones, String email) {
		listaTelefones = new ArrayList<String>();
		listaReivindicacoes = new ArrayList<Reivindicacao>();
		this.nome = nome;
		this.listaTelefones = telefones;
		this.email = email;
	}
	
	public ArrayList<String> getTelefones() {
		return listaTelefones;
	}
	public void setTelefones(ArrayList<String> telefones) {
		this.listaTelefones = telefones;
	}

	public ArrayList<Reivindicacao> getReivindicacoes() {
		return listaReivindicacoes;
	}
	public void setReivindicacoes(ArrayList<Reivindicacao> reivindicacoes) {
		this.listaReivindicacoes = reivindicacoes;
	}

	public Double getNumeroPostagens() {
		return numeroPostagens;
	}
	public void setNumeroPostagens(Double numeroPostagens) {
		this.numeroPostagens = numeroPostagens;
	}
	
	public Double getNumeroAvaliacaoPositiva() {
		return numeroAvaliacaoPositiva;
	}
	public void setNumeroAvaliacaoPositiva(Double numeroAvaliacaoPositiva) {
		this.numeroAvaliacaoPositiva = numeroAvaliacaoPositiva;
	}
	
	public Double getNumeroAvalicaoNegativa() {
		return numeroAvalicaoNegativa;
	}
	public void setNumeroAvalicaoNegativa(Double numeroAvalicaoNegativa) {
		this.numeroAvalicaoNegativa = numeroAvalicaoNegativa;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getPosicaoSocial() {
		return posicaoSocial;
	}
	public void setPosicaoSocial(String posicaoSocial) {
		this.posicaoSocial = posicaoSocial;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	public void adicionaReivindicao (Reivindicacao umaReivindicacao) {
		listaReivindicacoes.add(umaReivindicacao);
	}
	
}
