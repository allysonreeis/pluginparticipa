package model;

public class Reivindicacao {
	private String titulo;
	private String conteudo;
	private String categoria;
	private Double numeroCurtidas;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Double getNumeroCurtidas() {
		return numeroCurtidas;
	}
	public void setNumeroCurtidas(Double numeroCurtidas) {
		this.numeroCurtidas = numeroCurtidas;
	}
	
}
